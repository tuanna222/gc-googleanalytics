# Google Analytics Plugin for Game Closure SDK

This plugin allows you to collect analytics using the [Google Analytics](https://www.google.com/analytics/) toolkit.  Both iOS and Android targets are supported.

## Usage

Install the addon with `devkit install ssh://git@bitbucket.org/tuanna222/gc-googleanalytics.git`.

To specify your game's Tracking ID, edit the `manifest.json "android" and "ios" sections as shown below:

~~~
	"android": {
		"googleTrackingID": "UA-42399544-1"
	},
~~~

~~~
	"ios": {
		"googleTrackingID": "UA-42399545-1"
	},
~~~

Note that the manifest keys are case-sensitive.

To use Google Analytics tracking in your game, import the googleanalytics object:

~~~
import googleanalytics;
~~~

Then send individual track events like this:

~~~
googleAnalytics.track("myEvent", {
	"score": 999,
	"coins": 11,
	"isRandomParameter": true
});
~~~